## What is a URL? (Uniform Resource Locator)

If you’ve used the internet, you’ve used a URL before. A URL is predominantly an instruction on how to access a resource on the internet.  
The below image shows what a URL looks like with all of its features (it does not use all features in every request).

![alt](https://static-labs.tryhackme.cloud/sites/howhttpworks/newurl.png)

Scheme: This instructs on what protocol to use for accessing the resource such as HTTP, HTTPS, FTP (File Transfer Protocol).  

User: Some services require authentication to log in, you can put a username and password into the URL to log in.  

Host: The domain name or IP address of the server you wish to access.  

Port: The Port that you are going to connect to, usually 80 for HTTP and 443 for HTTPS, but this can be hosted on any port between 1 - 65535.  

Path: The file name or location of the resource you are trying to access.  

Query String: Extra bits of information that can be sent to the requested path. For example, /blog?id=1 would tell the blog path that you wish to receive the blog article with the id of 1.

Fragment: This is a reference to a location on the actual page requested. This is commonly used for pages with long content and can have a certain part of the page directly linked to it, so it is viewable to the user as soon as they access the page.

## Making a Request

It's possible to make a request to a web server with just one line "GET / HTTP/1.1"

![alt](https://static-labs.tryhackme.cloud/sites/howhttpworks/line.png)

But for a much richer web experience, you’ll need to send other data as well. This other data is sent in what is called headers, where headers contain extra information to give to the web server you’re communicating with, but we’ll go more into this in the Header task.  

## HTTP Methods

### GET Request

This is used for getting information from a web server.

### POST Request  

This is used for submitting data to the web server and potentially creating new records  

### PUT Request  

This is used for submitting data to a web server to update information  

### DELETE Request  

This is used for deleting information/records from a web server.  


## HTTP Status Codes:

### 100-199 - Information Response
### 200-299 - Success
### 300-399 - Redirection	
### 400-499 - Client 
### 500-599 - Server Errors

## Common HTTP Status Codes:
### 200 - OK	
### 201 - Created
### 301 - Permanent Redirect
### 302 - Temporary Redirect
### 400 - Bad Request	
### 401 - Not Authorised
### 403 - Forbidden	
### 405 - Method Not Allowed
### 404 - Page Not Found
### 500 - Internal Service Error
### 503 - Service Unavailable

## Common Request Headers
  
These are headers that are sent from the client (usually your browser) to the server.  
  
#### Host:
Some web servers host multiple websites so by providing the host headers you can tell it which one you require, otherwise you'll just receive 
the default website for the server.  
  
#### User-Agent:
This is your browser software and version number, telling the web server your browser software helps it format the website properly for 
your browser and also some elements of HMTL, JavaScript and CSS are only available in certain browsers.  
  
#### Content-Length:
When sending data to a web server such as in a form, the content length tells the web server how much data to expect in the web request.
This way the server can ensure it isn't missing any data.  
  
#### Accept-Encoding:
Tells the web server what types of compression methods the browser supports so the data can be made smaller for transmitting over the internet.  
  
  
#### Cookie:
Data sent to the server to help remember your information (see cookies task for more information).  
## Common Response Headers  
  
These are the headers that are returned to the client from the server after a request.  
  
#### Set-Cookie: 
Information to store which gets sent back to the web server on each request (see cookies task for more information).  
  
#### Cache-Control:
How long to store the content of the response in the browser's cache before it requests it again.  
  
#### Content-Type:
This tells the client what type of data is being returned, i.e., HTML, CSS, JavaScript, Images, PDF, Video, etc. Using the content-type header the 
browser then knows how to process the data.  
  
#### Content-Encoding:
What method has been used to compress the data to make it smaller when sending it over the internet.   

## Cookies

Cookies are saved when you receive a "Set-Cookie" header from a web server. 
Then every further request you make, you'll send the cookie data back to the web server. 
Because HTTP is stateless (doesn't keep track of your previous requests), cookies can be used to remind the web server who you are, some personal
settings for the website or whether you've been to the website before. Let's take a look at this as an example HTTP request:
![alt](https://static-labs.tryhackme.cloud/sites/howhttpworks/cookie_flow.png)
