## DNS
![alt](https://assets.tryhackme.com/additional/dnsindetail/ip2domaindrawing.png)

## Domain Hierarchy
![alt](https://assets.tryhackme.com/additional/dnsindetail/domain_levels.png)


1) What is the maximum length of a subdomain?<br/>
63<br/>
2) Which of the following characters cannot be used in a subdomain ( 3 b _ - )? -<br/>
_ can only use a-z 0-9 and hyphens (cannot start or end with hyphens or have consecutive hyphens)<br/>
3) What is the maximum length of a domain name? -<br/>
253<br/>
4) What type of TLD is .co.uk?<br/>
ccTLD<br/>

## DNS Record Types
A Record        | IPv4: 104.26.10.229<br/>
AAAA Record     | IPv6: 2606:4700:20::681a:be5<br/>
CNAME Record    | Resolve Another domain name<br/>
MX Record       | Resolve to the address of the servers that handle the email for the domain you are querying.<br/>
TXT Record      | TXT records are free text fields where any text-based data can be stored<br/>

## What happens when you make a DNS request
![alt](https://assets.tryhackme.com/additional/dnsindetail/dns.png)<br/>

# Summary
## Need practice with nslookup
